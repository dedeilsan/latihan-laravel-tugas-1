<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        $nama = $request['nama_lengkap'];
        $alamat = $request['address'];
      
        return view('halaman.home', compact('nama', 'alamat'));
    }
}
